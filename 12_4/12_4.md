**Задание 1: Подготовить инвентарь kubespray**

Новые тестовые кластеры требуют типичных простых настроек. Нужно подготовить инвентарь и проверить его работу. Требования к инвентарю:

подготовка работы кластера из 5 нод: 1 мастер и 4 рабочие ноды;
в качестве CRI — containerd;
запуск etcd производить на мастере.

**Решение**

Изменил параметр на container_manager: [containerd](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/containerd.md)

Готовый [inventory.ini](12_4/inventory.ini)

За основу взят invetory kubespray: [inventory.ini](https://github.com/kubernetes-sigs/kubespray/blob/master/inventory/sample/inventory.ini)

